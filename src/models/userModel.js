const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const User = new Schema({
  username: {type: String, required: true, unique: true},
  createdDate: {type: Date, required: true, default: Date.now()},
  password: {type: String, required: true},
});
module.exports = mongoose.model('User', User);
