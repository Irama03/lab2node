const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Note = new Schema({
  userId: {type: Schema.Types.ObjectId, required: true},
  completed: {type: Boolean, required: true, default: false},
  text: {type: String, required: true},
  createdDate: {type: Date, required: true, default: Date.now()},
});
module.exports = mongoose.model('Note', Note);
