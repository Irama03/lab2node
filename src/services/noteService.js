/* eslint-disable require-jsdoc */
const Note = require('../models/noteModel');

async function getNotes(userId, offset, limit) {
  const notes = await Note.find({userId: userId}, '-__v')
      .skip(offset).limit(limit);
  return [offset, limit, notes.length, notes];
}

async function getNoteById(id, userId) {
  const n = await Note.findById(id, '-__v');
  if (n) {
    if (userId === n.userId.toString()) return n;
    return 400;
  }
  return n;
}

async function createNote(userId, body) {
  if (!body.hasOwnProperty('text')) {
    return 'Please specify \'text\' parameter';
  }
  const newNote = new Note({
    userId: userId,
    text: body.text,
  });
  await newNote.save();
  return 'Success';
}

async function updateNote(id, userId, body) {
  if (!body.hasOwnProperty('text') || body.text.trim() === '') {
    return 'Please specify \'text\' parameter';
  }
  const n = await Note.findById(id);
  if (n) {
    if (userId === n.userId.toString()) {
      n.text = body.text;
      await n.save();
      return 'Success';
    }
    return 'This is note of another user';
  }
  return 'No note with id ' + id + ' found';
}

async function checkNote(id, userId) {
  const n = await Note.findById(id);
  if (n) {
    if (userId === n.userId.toString()) {
      n.completed = !n.completed;
      await n.save();
      return 'Success';
    }
    return 'This is note of another user';
  }
  return 'No note with id ' + id + ' found';
}

async function deleteNote(id, userId) {
  const n = await Note.findById(id);
  if (n) {
    if (userId === n.userId.toString()) {
      await Note.findByIdAndDelete(id);
      return 'Success';
    }
    return 'This is note of another user';
  }
  return 'No note with id ' + id + ' found';
}

module.exports =
    {getNotes, getNoteById, createNote, updateNote, checkNote, deleteNote};


