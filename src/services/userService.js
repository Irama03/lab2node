/* eslint-disable require-jsdoc */
const User = require('../models/userModel');
const bcrypt = require('bcrypt');
const Note = require('../models/noteModel');
const saltRounds = 10;

async function getUserById(id) {
  const u = await User.findById(id);
  if (u) {
    return {
      '_id': u._id,
      'username': u.username,
      'createdDate': u.createdDate,
    };
  }
  return u;
}

async function deleteUser(id) {
  const user = await User.findByIdAndDelete(id);
  if (user) {
    await Note.deleteMany({userId: id});
    return 'Success';
  }
  return 'There is no info about this user';
}

async function updateUserPassword(id, body) {
  if (!body.hasOwnProperty('oldPassword') || body.oldPassword.trim() === '') {
    return 'Please specify \'oldPassword\' parameter';
  }
  if (!body.hasOwnProperty('newPassword') || body.newPassword.trim() === '') {
    return 'Please specify \'newPassword\' parameter';
  }
  const user = await User.findById(id);
  if (user) {
    if (await bcrypt.compare(body.oldPassword, user.password)) {
      user.password = await bcrypt.hash(body.newPassword, saltRounds);
      await user.save();
      return 'Success';
    }
    return 'Incorrect old password';
  }
  return 'There is no info about this user';
}

module.exports = {getUserById, deleteUser, updateUserPassword};
