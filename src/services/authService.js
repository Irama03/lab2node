/* eslint-disable require-jsdoc */
const User = require('../models/userModel');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const saltRounds = 10;

async function createCredentials(body) {
  if (!body.hasOwnProperty('username') || body.username.trim() === '') {
    return 'Please specify \'username\' parameter';
  }
  if (!body.hasOwnProperty('password') || body.password.trim() === '') {
    return 'Please specify \'password\' parameter';
  }
  const oldUser = await User.findOne({username: body.username});
  if (oldUser) {
    return 'User already exists. Please login';
  }
  const passwordEncrypted = await bcrypt.hash(body.password, saltRounds);
  const newUser = new User({
    username: body.username,
    password: passwordEncrypted,
  });
  newUser.save();
  return 'Success';
}

async function loginWithCredentials(body) {
  if (!body.hasOwnProperty('username') || body.username.trim() === '') {
    return 'Please specify \'username\' parameter';
  }
  if (!body.hasOwnProperty('password') || body.password.trim() === '') {
    return 'Please specify \'password\' parameter';
  }
  const user = await User.findOne({username: body.username});
  if (!user) {
    return 'No user with such username found';
  }
  if (await bcrypt.compare(body.password, user.password)) {
    const token = jwt.sign(
        {
          user_id: user._id,
          username: body.username,
        },
        process.env.TOKEN_KEY,
        {
          expiresIn: '2h',
        },
    );
    return ['Success', token];
  } else return 'Incorrect password';
}

module.exports = {createCredentials, loginWithCredentials};
