const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const auth = require('../middlewares/authMiddleware');

const {getNotes, getNoteById, createNote, updateNote, checkNote, deleteNote} =
    require('../services/noteService');

router.get('/', auth, async (req, res) => {
  try {
    const userId = req.userId;
    let limit = 1000;
    let offset = 0;

    if (req.query.offset) {
      offset = parseInt(req.query.offset, 10);
      if (Number.isNaN(offset) || offset < 0) {
        res.status(400).json({message: 'Offset should be a number'});
        return;
      }
    }
    if (req.query.limit) {
      limit = parseInt(req.query.limit, 10);
      if (Number.isNaN(limit)) {
        res.status(400).json({message: 'Limit should be a number'});
        return;
      }
    }
    const notes = await getNotes(userId, offset, limit);
    res.json({
      offset: notes[0],
      limit: notes[1],
      count: notes[2],
      notes: notes[3],
    });
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.get('/:id', auth, async (req, res) => {
  try {
    const userId = req.userId;
    const note = await getNoteById(req.params.id, userId);
    if (note === 400) {
      res.status(400).json({message: 'This is note of another user'});
      return;
    }
    if (!note) {
      res.status(400)
          .json({message: 'No note with id ' + req.params.id + ' found'});
      return;
    }
    res.json({note});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.post('/', auth, async (req, res) => {
  try {
    const userId = req.userId;
    const message = await createNote(userId, req.body);
    if (message === 'Success') {
      res.status(200).json({message: message});
    } else res.status(400).json({message: message});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.put('/:id', auth, async (req, res) => {
  try {
    const userId = req.userId;
    const message = await updateNote(req.params.id, userId, req.body);
    if (message === 'Success') {
      res.status(200).json({message: message});
    } else res.status(400).json({message: message});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.patch('/:id', auth, async (req, res) => {
  try {
    const userId = req.userId;
    const message = await checkNote(req.params.id, userId);
    if (message === 'Success') {
      res.status(200).json({message: message});
    } else res.status(400).json({message: message});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

router.delete('/:id', auth, async (req, res) => {
  try {
    const userId = req.userId;
    const message = await deleteNote(req.params.id, userId);
    if (message === 'Success') {
      res.status(200).json({message: message});
    } else res.status(400).json({message: message});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
});

module.exports = router;
